package com.example.bellido.integrandoaplicaciones

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTabHost
import android.widget.TabHost
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : FragmentActivity() {

    lateinit var tabHost : FragmentTabHost

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tabHost = tabhost
        tabHost.setup(this, supportFragmentManager, tabcontent.id)

        var res : Resources = resources;

        var tab1 : TabHost.TabSpec = tabHost.newTabSpec("tab1")
        var tab2 : TabHost.TabSpec = tabHost.newTabSpec("tab2")
        var tab3 : TabHost.TabSpec = tabHost.newTabSpec("tab3")

        tab1.setIndicator(res.getString(R.string.eti_tab1), null)
        tab2.setIndicator(res.getString(R.string.eti_tab2), null)
        tab3.setIndicator(res.getString(R.string.eti_tab3), null)

        tabHost.addTab(tab1, fragment1::class.java, null)
        tabHost.addTab(tab2, fragment2::class.java, null)
        tabHost.addTab(tab3, fragment3::class.java, null)






    }
}
