package com.example.bellido.integrandoaplicaciones

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment2.*

class fragment2 : Fragment(), View.OnClickListener{

    val listaNumeros = mutableListOf<Int>()
    var resultado : Int = 0

    //VARIABLES DE RECURSOS (BOTONES)
    lateinit var  bt1 : Button
    lateinit var  bt2 : Button
    lateinit var  bt3 : Button
    lateinit var  bt4 : Button
    lateinit var  bt5 : Button
    lateinit var  bt6 : Button
    lateinit var  bt7 : Button
    lateinit var  bt8 : Button
    lateinit var  bt9: Button
    lateinit var  bt0 : Button
    lateinit var  btSum : Button
    lateinit var  btBorr : Button
    lateinit var  btIgu : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)

        var v : View = inflater!!.inflate(R.layout.fragment2, container, false)
        bt1 = v.findViewById(R.id.boton1)
        bt2 = v.findViewById(R.id.boton2)
        bt3 = v.findViewById(R.id.boton3)
        bt4 = v.findViewById(R.id.boton4)
        bt5 = v.findViewById(R.id.boton5)
        bt6 = v.findViewById(R.id.boton6)
        bt7 = v.findViewById(R.id.boton7)
        bt8 = v.findViewById(R.id.boton8)
        bt9 = v.findViewById(R.id.boton9)
        bt0 = v.findViewById(R.id.boton0)
        btSum = v.findViewById(R.id.botonSuma)
        btBorr = v.findViewById(R.id.botonBorrar)
        btIgu = v.findViewById(R.id.botonIgual)



        bt1.setOnClickListener(this)
        bt2.setOnClickListener(this)
        bt3.setOnClickListener(this)
        bt4.setOnClickListener(this)
        bt5.setOnClickListener(this)
        bt6.setOnClickListener(this)
        bt7.setOnClickListener(this)
        bt8.setOnClickListener(this)
        bt9.setOnClickListener(this)
        bt0.setOnClickListener(this)
        btSum.setOnClickListener(this)
        btBorr.setOnClickListener(this)
        btIgu.setOnClickListener(this)


        return v

    }

    override fun onClick(v: View) {
        when(v) {
            bt1 -> resultat.setText(resultat.text.toString().plus(1))
            bt2 ->  resultat.setText(resultat.text.toString().plus(2))
            bt3 ->  resultat.setText(resultat.text.toString().plus(3))
            bt4 ->  resultat.setText(resultat.text.toString().plus(4))
            bt5 ->  resultat.setText(resultat.text.toString().plus(5))
            bt6 ->  resultat.setText(resultat.text.toString().plus(6))
            bt7 ->  resultat.setText(resultat.text.toString().plus(7))
            bt8 ->  resultat.setText(resultat.text.toString().plus(8))
            bt9 ->  resultat.setText(resultat.text.toString().plus(9))
            bt0 -> if (resultat.text.toString() != ""){resultat.setText(resultat.text.toString().plus(0))}
            btSum -> {
                if (resultat.text.toString() == "") {
                    listaNumeros.add(0)
                }else {
                    listaNumeros.add(resultat.text.toString().toInt())
                    resultat.setText("")
                }
            }
            btIgu -> {
                if (resultat.text.toString() == "") {
                    listaNumeros.add(0)
                }else {
                    listaNumeros.add(resultat.text.toString().toInt())
                    resultat.setText("")
                }
                for (i in listaNumeros){
                    resultado += i

                }
                resultat.setText(resultado.toString())
                listaNumeros.clear()
                resultado=0;
            }

            botonBorrar -> {
                listaNumeros.clear()
                resultat.setText("")
            }
        }

    }

}