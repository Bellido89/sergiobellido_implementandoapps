package com.example.bellido.integrandoaplicaciones

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment3.*

class fragment3 : Fragment(), View.OnClickListener {


    lateinit var botonRojo : Button
    lateinit var boton0 : Button


    override fun onClick(v: View?) {
        when (v) {
            boton0 ->  entrada.setText(entrada.getText().toString().plus("0"));
            botonRojo -> salida.setText((entrada.text.toString().toFloat() * 2.0).toString())
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var v : View = inflater!!.inflate(R.layout.fragment3, container, false)
         botonRojo = v.findViewById(R.id.boton)
         boton0 = v.findViewById(R.id.boton0)

        botonRojo.setOnClickListener(this)
        boton0.setOnClickListener(this)
        return v

    }
}